\chapter{Présentation}

À l'origine, le terme \shell désigne un programme qui propose une
interface compréhensible à l'utilisateur pour accéder aux fonctions de
plus bas niveau du système d'exploitation. Le \shell masque la
complexité technique des appels système et du fonctionnement du noyau
pour permettre une utilisation aisée des fonctionnalités offertes par
un système d'exploitation.

Les fonctionnalités proposées par un \shell sont typiquement le
lancement d'autres programmes, la manipulation de fichiers,
l'automatisation de tâches…


\section{Différents types de \shell}

Il existe principalement deux types de \shell, à savoir~:
\begin{itemize}
  \item Les \shells textuels
  \item Les \shells graphiques
\end{itemize}

\subsection{Les \shells textuels}

Il en existe un grand nombre, dont voici quelques uns~: 
\begin{itemize}
\item Bourne shell (sh).
 	\begin{itemize}
	\item Almquist shell (ash).
		\begin{itemize}
		\item Debian Almquist shell (dash).
		\end{itemize}
	\item Bourne-Again shell (bash).
	\item Korn shell (ksh).
	\item Friendly interactive shell (fish).
    \end{itemize}
\item C shell (csh).
	\begin{itemize}
	\item TENEX C shell (tcsh).
	\end{itemize}
\item Es shell (es).
\item Esh (Unix) Easy shell.
\item RC shell (rc) - shell pour Plan 9 et Unix.
\item Runscript (Utilisé pour les scripts de lancement sous Gentoo).
\item Scsh (Scheme shell).
\item Stand-Alone shell (sash).
\item Z shell (zsh).
\end{itemize}

Ils sont les premiers apparus, et ils sont toujours régulièrement
utilisés par les programmeurs, les administrateurs système et les
passionnés.

Ce n'est pas par pur masochisme, mais en raison de leur nombreuses
qualités. Les interfaces de type \shell demandent moins de ressources,
sont très efficace à l'utilisation pour qui les connait, permettent
d'automatiser facilement les activités récurrentes.

Les plus courants sont tous les dérivés du \textbf{Bourne shell} et le
\textbf{C shell}. Il existe une norme (POSIX) qui définit un jeu de
fonction assurant une certaine «portabilité» entre ces différents
\shell.

\subsection{Les \shells graphiques}

Un \shell graphique est un programme qui propose une interface
graphique à l'utilisateur. Cette interface présente généralement les
applications sous forme de fenêtres et l'utilisateur peut se servir
d'un outil de pointage comme une souris ou un écran tactile pour
interagir avec la machine.

Sur un système GNU/Linux, ont pourrait les classer dans trois catégories~:
\begin{description}
\item[Les simples gestionnaires de fenêtres~:]
  ceux-ci n'ont d'autre fonctions que d'organiser les fenêtres pour
  permettre à l'utilisateur de faire fonctionner plusieurs
  applications à un moment donné.\par
  Exemples~:\par
  \begin{itemize}
    \item BlackBox
    \item FluxBox
  \end{itemize}
\item[Les \shells graphiques à proprement parler~:] ils incluent en
  plus des fonctions de base de manipulation des fenêtres un certain
  nombre d'outils pour la manipulation des fichiers.\par
  Exemples~:\par
  \begin{itemize}
    \item Enlightenment DR17
  \end{itemize}
\item[Les environnements de bureau complets~:] ces derniers intègrent
  en plus des éléments déjà cités toute une panoplie d'applications --
  client de courriel, navigateur internet, suite bureautique,
  messagerie instantanée -- basées sur des technologies communes et
  proposant une interface cohérente.\par
  Exemples~:\par
  \begin{itemize}
    \item GNOME
    \item KDE
    \item XFCE
  \end{itemize}
\end{description}

\section{\Shell\unskip, terminal, qu'es aquò~?}
Les deux appellations \shell et terminal\footnote{Cette
  différenciation d'un niveau plus avancé peut être ignorée sans
  vergogne lors d'une première lecture} sont souvent utilisées sans
distinction, mais ces deux termes désignent en réalité des concepts
bien différents~:
\begin{description}
\item[terminal] Dans les débuts de l'informatique, les ordinateurs
  étaient d'énormes systèmes qui occupaient des pièces entières. Les
  utilisateurs, ne pouvant avoir physiquement accès à ces machines,
  s'y connectaient grâce à des équipements simples constitués
  uniquement d'un écran et d'un clavier, sans capacités de calcul. Ces
  équipement terminaux ont laissé leur nom.\par
  Dans un système GNU/Linux moderne un terminal n'est plus un
  équipement informatique mais seulement un programme comme le
  \texttt{gnome-terminal} de Gnome ou la \texttt{konsole} de KDE, qui
  peut être lancés aux côtés de nombreuses autres applications.
\item[\shell] Le \shell est bien celui qui nous intéresse. C'est le
  programme capable d'\emph{interpréter} les commandes saisies par
  l'utilisateur, d'\emph{exécuter} les opérations correspondantes et
  de \emph{fournir le résultat}. Le programme de \shell tourne dans un
  terminal, qui sert d'intermédiaire technique avec pour fonction de
  transmettre les touches frappées par l'utilisateur au programme de
  \shell, et de présenter à l'utilisateur le résultat retournés par le
  \shell.
\end{description}

\section{Au programme}
Pendant cet atelier, nous aborderons essentiellement \shells textuels,
et plus particulièrement le \shell \emph{bash} installé par défaut
avec Ubuntu. Ce \shell est accessible par exemple à travers
l'émulateur de terminal \texttt{gnome-terminal} qui se trouve dans le
menu $Applications\rightarrow Accessoires\rightarrow Terminal$.

Nous ferons tout de même allusion aux environnements graphiques pour
essayer d'établir quelques parallèles entre les deux approches.


\chapter{Introduction au \shell textuel}

\section{L'invite de commande}
L'invite de commande (ou \og prompt\fg\ en anglais) est la première
chose que l'on voit en se connectant à un terminal. Elle est
constituée des quelques caractères qui précèdent le curseur et donne à
l'utilisateur des informations sur l'état du \shell. Elle indique
notamment que celui-ci est prêt à recevoir une saisie au clavier.

Lorsqu'on exécute une commande très longue comme une recherche de
fichier, il faut attendre le retour de l'invite avant de saisir la
commande suivante. Tant que le \shell n'a pas réaffiché cette invite,
c'est qu'il n'a pas terminé l'exécution de la commande précédente.

L'invite contient plusieurs informations intéressantes pour
l'utilisateur. Sa forme exacte peut varier d'un système à l'autre,
mais l'invite habituelle pour un \shell \emph{bash} est la
suivante~:\par
\prompt{echo 'Hello world'}

Le dernier caractère de l'invite peut être soit \verb+#+ si vous êtes
administrateur de la machine, ou \verb+$+ si vous êtes simple
utilisateur. Comme nous le verrons plus en détail par la suite
l'utilisateur simple ne peut modifier que son dossier personnel, alors
qu'un administrateur pourra modifier l'ensemble du système.

\begin{center}
{\includegraphics[width=9.9cm]{invite_de_commande}}

\emph{Sur cet exemple, nous voyons que l'utilisateur connecté au
  terminal est nommé «\texttt{cyril}», qu'il n'est pas administrateur
  de la machine, et que le dossier courant est le sous-dossier
  «\texttt{Images}» de son dossier personnel.}
\end{center}

L'invite de commande peut être entièrement personnalisée pour afficher
d'autres informations telles que l'heure, l'utilisation du processeur,
le niveau de charge de la batterie pour un ordinateur portable, mais
cela dépasse le cadre de ce cours.

Il est également à noter qu'on recommande fortement de se connecter
systématiquement comme utilisateur simple sans droits
d'administration, et qu'on emprunte ensuite temporairement ces droits
lorsque c'est nécessaire.

\section{Formation d'une commande}
Le nombre de commandes et de leurs arguments est vaste, mais les
règles de formation d'une commande restent relativement simples. Une
commande est constituée d'un nom de commande (suite de caractères sans
espaces), suivi éventuellement d'un ou plusieurs options et arguments
séparés par des espaces.

Ainsi~:
\begin{listeboules}
\item \prompt{dvd+rw-format} est une seule commande, sans arguments.
\item \prompt{ls -l -a} représente l'appel de la commande \texttt{ls}
  avec deux paramètres \texttt{-l} et \texttt{-a}.
\end{listeboules}

\subsection{Options et arguments}
Lorsqu'on utilise une commande non triviale qui comporte plusieurs
paramètres, il est intéressant pour une meilleure compréhension de
différentier les options et les arguments parmi ceux-ci.

\begin{listeboules}
\item[options] Les options permettent de modifier le fonctionnement
  d'une commande, pour l'adapter à un besoin précis. La commande à
  toujours le même but, mais l'ajout d'une option peut permettre de
  modifier légèrement son fonctionnement pour correspondre à ce que
  l'utilisateur recherche à un moment donné. Les options répondent à
  la question \og Comment vais-je travailler~?\fg.
\item[arguments] Les arguments indiquent une commande l'objet de son
  travail. Fournir des arguments répond à la question \og Sur quoi
  dois-je travailler~?\fg.
\end{listeboules}

Par exemple, considérons les commandes suivantes~:
\begin{listeboules}
\item \prompt{ls /}
\item \prompt{ls -l /}
\item \prompt{ls -l /home}
\end{listeboules}

~\\
La commande \texttt{ls} permet d'afficher le contenu d'un
répertoire. Elle accepte une \emph{option} \texttt{-l} qui lui demande
d'afficher une liste d'informations détaillées pour chaque
fichier. Elle prend par ailleurs comme \emph{argument} le nom du
dossier dont on veut afficher le contenu.

Généralement les options sont précédées d'un ou deux caractères \og -
\fg. On préfère en général écrire les options en premier suivies des
argument, mais ça n'est pas une règle universelle. Certaines commandes
peuvent avoir un comportement spécifique, qui est alors décrit dans
leur documentation.

\subsection{Options courtes, options longues}
La plupart des options ont une forme courte et une forme longue. Par
exemple les deux formes suivantes sont de parfaits synonymes~:
\begin{listeboules}
\item \prompt{ls -a}
\item \prompt{ls -{}-all}
\end{listeboules}

La forme courte est plus rapide à taper, et la forme longue
généralement plus facile à retenir, mais elles ont toutes deux
strictement la même signification. Chacun est donc libre d'utiliser la
forme qui lui plaît, sans distinction.

Remarquons que les options comme les commandes sont sensibles à la
casse. Ainsi \texttt{ls -x} et \texttt{ls -X} ont deux comportements
bien distincts.

\subsection{Options accollées}
Sous leur forme courte, les options sont également librement
concaténables. Ainsi les deux formes suivantes sont rigoureusement
équivalentes :
\begin{listeboules}
\item \prompt{ls -a -l} 
\item \prompt{ls -al}
\end{listeboules}

\subsection{Options avec paramètre}
Enfin, certaines options exigent un paramètre qui va préciser encore
la portée ou le fonctionnement de l'option en question.

Généralement cela se présente sous la forme~:
\begin{listeboules}
\item \prompt{ls -T TAILLE} ou
\item \prompt{ls -{}-tabsize=TAILLE}.
\end{listeboules}

Même dans la première forme où l'option a un nom court, on ne peut pas
l'accoler avec d'autres options courtes, du fait qu'elle a besoin d'un
paramètre.

\chapter{Utilisation}

\section{Lancement d'un programme}
\subsection{Exemple}
Un \shell textuel ne comporte pas d'icônes ou de menus. La méthode
pour lancer un programme consiste à taper son nom et à valider avec la
touche \verb+<Entrée>+.

Par exemple pour lancer le navigateur Firefox, il suffit de taper son
nom (en minuscules)~: \texttt{firefox}, puis d'appuyer sur
\verb+<Entrée>+.

L'avantage de ce mode d'opération est qu'il est possible de lancer
immédiatement n'importe quel programme sans avoir besoin de chercher
son icône dans un obscur menu ou un coin perdu du
bureau. L'inconvénient est qu'il faut connaitre les noms des
programmes que l'on souhaite lancer.

\subsection{Complétion des commandes}
Pour nous simplifier la vie, le \shell dispose d'une fonctionnalité
dite de \og complétion\fg, qui se charge de compléter automatiquement
les commandes dont le début ne laisse aucune ambiguité, ou d'afficher
les alternatives disponibles le cas échéant. Dans un \shell
\emph{bash}, la complétion se déclenche avec la touche de tabulation.

Si l'on reprend l'exemple de Firefox, plutôt que de taper le nom en
entier, il suffit de taper les 4 premières lettres \texttt{fire}
suivies de la touche \verb+<Tab>+ pour que le \shell complète
automatiquement le nom en \texttt{firefox}.

Dans le cas où l'on demande la complétion alors qu'il y a plusieurs
possibilités, le premier appui sur \verb+<Tab>+ ne provoque aucune
réaction. Il faut alors appuyer une deuxième fois sur \verb+<Tab>+
pour que le \shell propose l'ensemble des commandes qui correspondent
aux caractères déjà tapés.

Par exemple si l'on tape seulement \texttt{fi} à l'invite de commande,
puis deux fois \verb+<Tab>+, le \shell nous indique l'ensemble des
commandes dont le nom commence par \og fi\fg.
\begin{verbatim}
cyril@bendigo:~$ fi
fi           file-roller  findfs       firefox      
file         find         findsmb      firefox-3.5  
filefrag     find2perl    finger
cyril@bendigo:~$ fi
\end{verbatim}

Si deux appuis sur \verb+<Tab>+ n'ont pas d'effet, c'est que le \shell
ne connait aucune commande commençant par les caractères demandés.

\section{Navigation dans le système de fichiers}
\subsection{Le système de fichiers}
Dans le monde Unix, le système de fichiers est une arborescence qui a
une unique racine, le dossier \texttt{/}. Tous les autres dossiers
sont des sous-dossiers de cette racine. Par exemple le dossier
contenant les données des utilisateurs a pour chemin \texttt{/home}.

Chaque niveau d'arborescence est ensuite séparé par un caractère
\texttt{/}. Par exemple le dossier de l'utilisateur \texttt{cyril} a
pour chemin \texttt{/home/cyril}, et son dossier d'images
\texttt{/home/cyril/Images}.

Par ailleurs, le nom de dossier \texttt{\textasciitilde} (tilde) est
un synonyme pour le nom du dossier personnel de l'utilisateur
courant. Si mon nom d'utilisateur est \texttt{cyril},
\texttt{\textasciitilde} a donc la même signification pour le \shell
que \texttt{/home/cyril}.

Les fichiers dont le nom commence par un \texttt{.} sont cachés.

\textbf{Remarque~:}

Comme le système de fichiers Unix est constitué d'une arborescence
unique, les médias amovibles tels que les cédéroms et clefs USB sont
eux aussi attachés (on dit \og montés\fg) à un endroit de cette
arborescence.

Par exemple le lecteur de cédérom est généralement attaché au chemin
\texttt{/media/cdrom} et une clef USB se trouvera souvent montée dans
le dossier \texttt{/media/disk}.

\subsection{Norme d'organisation}
L'organisation es fichiers nécessaires au fonctionnement d'un système
d'exploitation est normalisée par le modèle appelé \og Filesystem
Hierarchy Standard\fg souvent abrégé FHS, dont l'acronyme signifie
simplement \og Standard pour la hiérarchie du système de fichiers\fg.

Cette norme décrit un certain nombre de pratiques à respecter pour une
organisation intelligente des fichiers système et utilisateur, ainsi
qu'une série de noms de dossiers standard à utiliser.

Voici quelques exemples de dossiers décrits par le standard FHS~:
\begin{listeboules}
\item \texttt{/bin} et \texttt{/sbin}~: programmes essentiels au
  fonctionnement du système. \texttt{/sbin} contient plus
  particulièrement ceux réservés au super-utilisateur.
\item \texttt{/proc}, \texttt{/sys} et \texttt{/dev}~: ces dossiers
  contiennent une arborescence virtuelle qui représente l'état du
  système. Les fichiers et dossiers correspondants ne sont pas
  enregistrés sur le disque dur, mais créés et modifiés à la volée par
  le noyau pour rentre compte de son état et de celui des
  périphériques.
\item \texttt{/usr}~: programmes nécessaires aux utilisateurs et non
  essentiels au fonctionnement du système.
  \begin{listeboules}
  \item \texttt{/usr/bin} et \texttt{/usr/sbin}~: programmes non
    essentiels, respectivement ceux accessibles à tous et ceux
    accessibles uniquement au super-utilisateur.
  \item \texttt{/usr/share} données des programmes non essentiels.
  \end{listeboules}
\item \texttt{/mnt} et \texttt{/media}~: utilisés pour les montages
  temporaires. \texttt{/media} est le dossier où seront montés les
  médias amovibles tels que les clefs USB, les CD et DVD, les disques
  durs externes ou les lecteurs de musique. \texttt{/mnt} est lui
  réservé à l'administrateur pour effectuer des montages manuels et
  temporaires.
\item \texttt{/opt} et \texttt{/srv}~: ces deux répertoires
  contiennent des programmes et données spécifiques à une machine en
  particulier. \texttt{/srv} s'utilise pour du contenu dont la machine
  est serveur, comme par exemple un site Web. \texttt{/opt} peut être
  utilisé pour l'installation manuelle de logiciels absents de la
  distribution.
\item \texttt{/var}~: données qui varient au cours de l'utilisation du
  système. Cela s'oppose aux programmes installés qui vont très peu
  changer, généralement uniquement lors de mises à jour.
\item \texttt{/var/log}~: emplacement des fichiers journaux du
  système. C'est ici que l'on peut retrouver les traces de
  fonctionnement de noyau et des programmes, pour par exemple analyser
  une erreur.
\item \texttt{/etc}~: dossier contenant les paramètres de
  configuration globaux des programmes installés. Les utilisateurs
  peuvent ensuite éventuellement avoir une configuration spécifique
  que est alors stockée dans leur dossier personnel.
\item \texttt{/home}~: données personnelles des utilisateurs. On
  attribue à chaque utilisateur un sous-dossier en utilisant son
  identifiant, par exemple \texttt{/home/toto}.
\end{listeboules}
~
\\
\begin{tikzpicture}[scale=.7,level distance=25mm,sibling distance=8mm]
  \node {/} [grow'=right]
  child {node {/bin}}
  child {node {/boot}}
  child {node {/etc}}
  child {node {/home}
    child[grow=north east] {node {\textasciitilde}
      child[grow=east] {node {\textasciitilde{}/Bureau}
        child[grow=east, level distance=35mm] {node {\textasciitilde{}/Bureau/examples.desktop}}}
      child[level distance=15mm] {node {\textasciitilde{}/Images}}}
    child[grow=south east] {node {/home/guest}
      child[grow=south east] {node {/home/guest/Bureau}}
      child[grow=north east, level distance=30mm] {node {/home/guest/Images}}}}
  child {node {/lib}}
  child {node {/mnt}}
  child {node {/media}}
  child {node {/proc}}
  child {node {/sbin}}
  child {node {/sys}}
  child {node {/usr}
    child {node {/usr/bin}}
    child {node {/usr/sbin}}
    child {node {/usr/share}}}
  child {node {/var}
    child[level distance=15mm,grow=south east] {node {/var/log}}
  };
\end{tikzpicture}

    
\subsection{Navigation}
La navigation dans le systèmes de fichiers est la base du \shell. Voici les deux
commandes incontournables pour ce faire, auxquelles on adjoint généralement un
\textbf{chemin}~:
\begin{listeboules}
  \item Pour lister le contenu d'un dossier~: \texttt{ls} [chemin du dossier].
  \item Pour se déplacer dans un dossier~: \texttt{cd} [chemin du dossier].
\end{listeboules}

Le \shell sous Unix est l'héritage d'une histoire assez ancienne. Cela
a longtemps été la seule interface entre l'homme et la machine à une
époque ou on ne pouvait envoyer que quelques caractères à la
seconde. De ce fait, la plus part des commandes de navigation dans les
fichiers sont courtes et facilement mémorisables : \texttt{LiSt
  <dossier>} ou \texttt{Change Directory <dossier>}.

Le chemin (vous rencontrerez peut-être parfois \emph{path} en anglais) est
l'adresse permettant de désigner un fichier ou un dossier dans l'arborescence du
système de fichiers.

Un chemin peut être soit~:
\begin{listeboules}
  \item \emph{absolu} lorsqu'il commence par un caractère \texttt{/}. Il s'agit
    alors d'un chemin qui décrit sans ambiguité un élément précis du système de
    fichiers. Par exemple \texttt{/home/cyril} est un chemin absolu qui désigne
    le dossier personnel de l'utilisateur \texttt{cyril}.
  \item \emph{relatif} s'il ne commence pas par un caractère \texttt{/}. Un
    chemin relatif désigne un emplacement relativement au dossier courant
    (signalé par l'invite de commande).
\end{listeboules}

Par exemple \texttt{Images} est un chemin relatif qui peut désigner le dossier
d'images de l'utilisateur \texttt{cyril} si je me trouve dans son dossier
personnel~:
\begin{verbatim}
cyril@bendigo:~$ ls Images/
logo-ubuntu.png  ubuntu-party.png
cyril@bendigo:~$
\end{verbatim}

Ou bien le dossier images de l'utilisateur \texttt{aurelien} si je me trouve
dans son dossier personnel~:
\begin{verbatim}
cyril@bendigo:/home/aurelien$ ls Images/
photo-identité.png  vacances-antarctic.png
cyril@bendigo:/home/aurelien$
\end{verbatim}

Quand aucun nom ou chemin de dossier est indiqué, le comportement des deux
commandes précédentes est modifié~:
\begin{listeboules}
  \item \texttt{ls} liste le contenu du dossier courant.
  \item \texttt{cd} se rend dans le dossier personnel de l'utilisateur
    courant.
\end{listeboules}

\subsection{Chemins particuliers}

Voici quelques chemins particuliers qui peuvent être utilisés avec les commandes
de \shell\unskip~:
\begin{listeboules}
  \item Le dossier courant est spécifié par le point \og\texttt{.}\fg.
  \item Le dossier parent dans l'arborescence est spécifié par deux points
    \og\texttt{..}\fg.
  \item Le dossier précédent dans la navigation est spécifié par le tiret
    \og\texttt{-}\fg.
\end{listeboules}

\subsection{Où suis-je~?}
Lorsqu'on navigue dans le système de fichiers, de temps en temps, il est souvent
nécessaire de vérifier à quel endroit on est situé, afin d'éviter de faire des
erreurs de manipulation.

Pour cela, il y a deux façons de procéder. On peut utiliser une commande, nommée
\texttt{pwd} ou bien lire directement le dossier courant dans l'invite de
commande.

Contrairement à l'invite, \texttt{pwd} affiche toujours le chemin complet du
dossier courant. Si vous vous situez dans votre dossier utilisateur, l'invite
affichera comme dossier courant \texttt{\textasciitilde} mais la commande
\texttt{pwd} vous retournera bien le chemin complet
\texttt{/home/nom\_d\_utilisateur}.

\subsection{Complétion des chemins}
De même que pour les commandes, le \shell dispose d'une capacité de complétion
des chemins, qui s'active également avec la touche de tabulation.

Par exemple pour se rendre dans le dossier \texttt{/home} il suffit de taper
\texttt{cd /h} suivi d'un appui sur \verb+<Tab>+ pour que le \shell complète
automatiquement la commande en \texttt{cd /home}.

Lorsque le chemin qu'on souhaite compléter peut correspondre à plusieurs
éléments, le premier appui sur \verb+<Tab>+ n'a pas d'effet, et un deuxième
appui affichera toute les solutions possibles. Par exemple si l'on tape
\texttt{cd /s} suivi de deux appui sur \verb+<Tab>+, le \shell nous affiche tous
les fichiers et dossier contenus dans la racine / et dont le nom commence par un
s~:
\begin{verbatim}
cyril@bendigo:~$ cd /s<Tab><Tab>
sbin/    selinux/ srv/     sys/     
cyril@bendigo:~$ cd /s
\end{verbatim}


\section{Ouverture et lecture des fichiers}

Afin d'effectuer ces actions, il existe toute une ribambelle de programmes, mais ici, nous ne verrons que les six plus courants, à savoir \texttt{cat}, \texttt{less}, \texttt{more}, \texttt{head} et \texttt{tail}.

Tous ces programmes, à l'exception de tee, fonctionnent en prenant en argument le nom/chemin du fichier à lire.

\texttt{cat} affiche le fichier directement dans le terminal.

\texttt{less} et \texttt{more} permettent de lire le contenu du fichier progressivement.

La différence entre les deux est que \texttt{more} charge l'intégralité du fichier en mémoire, alors que \texttt{less} charge le fichier au fur et a mesure de l'affichage, de plus, \texttt{less} permet de remonter dans le fichier.\par
Pour être bref, si vous devez ouvrir un fichier texte volumineux,
utilisez plutôt \texttt{less}\footnote{Comme son nom l'indique, less
  est mieux que more !}.

Aussi bien dans \texttt{less} que \texttt{more}, l'utilisation de la
touche \texttt{h} vous donne accès à une aide sur la navigation à
l'intérieur du fichier, ce qui peut s'avérer fort pratique.

\texttt{head} et \texttt{tail} permettent de visionner qu'une partie du fichier.

\texttt{head} affiche par défaut les 10 premières lignes du fichier.

\texttt{tail} affiche par défaut les 10 dernières lignes du fichier.

On peut personnaliser un peu ces commandes en ajoutant le nombre de lignes à afficher en argument~:
\begin{itemize}
\item \texttt{head -30 fichier}
\item \texttt{tail -30 fichier}
\end{itemize}
Elles afficheront alors le nombre de lignes demandées en début/fin de fichier respectivement.

Pour \texttt{tail}, avec l'option \emph{-f}, il peut afficher le
fichier \og en direct\fg, en affichant les lignes au fur et à mesure
qu'elles sont écrites dans le fichier.
\newpage
\section{Manipulation des fichiers}

\subsection{Copie et déplacement}

\subsubsection{Méthodes courantes}

Les deux commandes principales que nous allons utiliser ici sont les
suivantes~:
\begin{itemize}
\item Pour copier un fichier~: \texttt{cp} [source] [destination].
\item Pour déplacer ou renommer un fichier~: \texttt{mv} [source]
[destination].
\end{itemize}

Concernant \texttt{mv}, le déplacement et le renommage sont deux
principes si proches sous GNU/Linux qu'ils sont compris dans la même
commande.

Le comportement de \texttt{mv} dépend en fait de la
nature de la destination.

\begin{itemize}
  \item Si [destination] n'existe pas, [source] est renommé
[destination].
  \item Si [destination] est un \emph{fichier} existant, [source] est
renommé [destination] et remplace alors le fichier [destination]
existant !
  \item Si [destination] est un \emph{dossier} existant, [source] est
déplacé dans le dossier [destination].
\end{itemize}

Voici quelques exemples concrets de déplacement et de renommage~:
\begin{itemize}
  \item Pour déplacer le fichier test, qui est dans le dossier courant,
vers le dossier /var/www/~:

\prompt{mv test /var/www/}
  \item Pour renommer le fichier test du dossier courant, en test2, lui
aussi dans le dossier courant~:

\prompt{mv test test2}
  \item Il est tout à fait possible de faire les deux en même
temps. Pour renommer le fichier test, qui est dans le dossier courant,
en test2 contenu dans /var/www/~:

\prompt{mv test /var/www/test2}.
\end{itemize}

\subsubsection{Méthode alternatives} Ici, vous verrez des méthodes
surprenantes pour copier des fichiers qui ne sont là qu'a titre
d'exemple. Il est fortement conseillé d'utiliser les méthodes
courantes. Ces méthodes permettent d'introduire des commandes
particulières et font appel à de nouveaux mécanismes.

\begin{itemize}
\item \prompt{cat test $>$ test2}
\item \prompt{cat test $|$ tee test2 $>$ /dev/null}
\end{itemize}

Les caractères \og$>$\fg\ et \og$|$\fg\ employés ici sont expliqués en
détail au paragraphe 3.6.

\subsection{Suppression}

Afin de supprimer des fichiers ou des dossiers, on utilise
principalement la commande \texttt{rm}.

Cette commande est cependant à utiliser avec précaution, car après
effacement, les données ne sont pas récupérables. Ce n'est pas un
déplacement dans la corbeille comme habituellement dans un
environnement graphique, mais une suppression pure et simple du
disque.

Quand il s'agit de supprimer un fichier, l'utilisation de \texttt{rm}
est triviale~: \texttt{rm lefichier}

Par mesure de précaution, \texttt{rm} n'est pas capable de supprimer
un dossier. Pour cela, il faut faire appel à la commande
\texttt{rmdir} qui permet de supprimer un dossier vide. Il est donc
nécessaire de vider le contenu d'un dossier avant de pouvoir supprimer
celui-ci.

Pour cela, on peut demander à \texttt{rm} de le faire, en lui
indiquant de supprimer récursivement les fichiers et dossier qu'il
rencontrera. 

Pour résumer, quand il doit supprimer une arborescence, \texttt{rm}
commence par les éléments les plus lointains (les \og feuilles\fg\ de
l'arbre) pour revenir ensuite vers les éléments plus proches de la
racine de l'arbre.  \newpage Exemple~:
\begin{center}

\begin{tikzpicture}[
    fichier/.style={rectangle, draw=none, rounded corners=1mm, fill=blue, drop shadow,
        text centered, anchor=north, text=white},
    repertoire/.style={circle, draw=none, fill=orange, circular drop shadow,
        text centered, anchor=north, text=white},
    leaf/.style={circle, draw=none, fill=red, circular drop shadow,
        text centered, anchor=north, text=white},
    level distance=2cm, growth parent anchor=south,
    sibling distance=2cm
]
\node (racine) [repertoire] {\texttt{/}} [->]
    child{ 
        node (fichier1) [leaf] {\texttt{vmlinuz}}
         }
    child{ 
      node (fichier4) [leaf] {\texttt{initrd.img}}
      }
    child{ [sibling distance=9cm] 
        node (repertoire000) [repertoire] {\texttt{home}}
        child{ [sibling distance=4cm]
            node (repertoire010) [repertoire] {\texttt{user0}}
            child { [sibling distance=2cm]
              node (repertoire100) [repertoire] {\texttt{Docs}}
              }
            child { [sibling distance=2cm] 
              node (repertoire200) [repertoire] {\texttt{Pics}}
              child { [sibling distance=1cm] 
                node (fichier2) [fichier] {\texttt{photo.jpg}}
                }
              child { [sibling distance=1cm]
                node (fichier3) [fichier] {\texttt{fond.png}}
                }
              }
            }
        child{ [sibling distance=4cm]
            node (repertoire020) [repertoire] {\texttt{user1}}
            }
         }
;

\end{tikzpicture}
\end{center}

Les lettres majuscules représentent des dossiers, les lettres
minuscules représentent des fichiers.

Cette commande possède un garde-fou sous la forme de l'option
\texttt{-i}. \texttt{rm -i} demande confirmation à chaque fois qu'il
entre dans un dossier ou qu'il va supprimer un élément. Ceci dit, il
faut penser à utiliser cette option pour profiter de sa protection.
Lorsque \texttt{rm} arrive sur un fichier qu'il ne peut pas supprimer,
par exemple s'il est protégé en écriture, il demande confirmation à
l'utilisateur pour supprimer tout de même ce fichier. Pour outrepasser
cette sécurité, il faut y adjoindre l'option \texttt{-f}, mais dans ce
cas, à la moindre erreur commise dans la commande, c'est fini, les
données sont effacées sans ménagement.

Donc, sur un forum, si un malin vous dit \og Pour traduire toutes vos
applications en français, tapez \texttt{rm~/~-fr}\fg, vous aurez de
quoi lui dire d'aller faire ses mauvaises blagues ailleurs.

De la même façon, une erreur fréquemment commise est la suivante : on
insère par mégarde un séparateur dans la commande et on tape
\prompt{rm~-rf~/~home/utilisateur\_a\_supprime} au lieu de
\prompt{rm~-rf~/home/utilisateur\_a\_supprime}. Le résultat peut
s'avérer assez tragique.

\subsection{Création d'un dossier}

La commande utilisée pour créer un dossier est \texttt{mkdir}~:

\prompt{mkdir~nouveau\_dossier}


Dans cette forme simple, elle permet de créer un dossier unique ; avec
l'ajout de l'option \texttt{-p}, vous créerez toute l'arborescence qui
mène à ce dossier.

Par exemple, imaginons qu'on souhaite créer un dossier K, et un
dossier L à l'intérieur de ce dossier K.

On veut donc arriver à l'arborescence suivante~:

\begin{center}

\begin{tikzpicture}[
    fichier/.style={rectangle, draw=none, rounded corners=1mm, fill=blue, drop shadow,
        text centered, anchor=north, text=white},
    repertoire/.style={circle, draw=none, fill=orange, circular drop shadow,
        text centered, anchor=north, text=white},
    leaf/.style={circle, draw=none, fill=red, circular drop shadow,
        text centered, anchor=north, text=white},
    level distance=2cm, growth parent anchor=south,
    sibling distance=2cm
]
\node (racine) [repertoire] {\texttt{K}} [->]
    child{ 
        node (fichier1) [repertoire] {\texttt{L}}
         }
;
\end{tikzpicture}

\end{center}

Pour cela, on peut taper au choix la séquence de
commandes suivantes~:
\begin{itemize}
\item \texttt{mkdir K}
\item \texttt{mkdir K/L}
\end{itemize}

Ou encore~:
\begin{itemize}
\item \texttt{mkdir K}
\item \texttt{cd K}
\item \texttt{mkdir L}
\end{itemize}

Avec l'option \texttt{-p}, il suffit de taper la
commande suivante pour créer les deux dossiers en une seule étape~:

\prompt{mkdir -p K/L}

\newpage


\section{Comment trouver de l'aide en ligne de commande~?}
\subsection{Man}

Il existe une bibliothèque d'aide très complète qu'on appelle «man»
(pour manuels).

Elle possède 9 sections, classées dans l'ordre suivant.

\begin{itemize}
\item 1. Commandes utilisateur
\item 2. Appels système
\item 3. Fonctions de bibliothèque
\item 4. Fichiers spéciaux
\item 5. Formats de fichier
\item 6. Jeux
\item 7. Divers
\item 8. Administration système
\item 9. Interface du noyau Linux
\end{itemize}

La commande pour appeler une page de manuel est~:

\begin{verbatim} man numéro_de_section nom_du_programme
\end{verbatim}

La plupart des commandes ont des pages de manuel, et elles sont même
quasi-obligatoires lors de la création des paquets .deb qui
constituent Debian et Ubuntu.

Dans la majeure partie des cas, vous pouvez omettre le numéro de
section comme dans~:
\begin{verbatim} man ls
\end{verbatim}

En effet, le numéro de section de la page de manuel est facultatif, et
par défaut, \texttt{man} vous affichera la page correspondant au
numéro de section le plus petit.  Il n'est réellement nécessaire de
préciser la section que pour les éléments qui ont des pages de manuel
dans plusieurs sections.

Exemples~:

\begin{verbatim} man man man 1 man man 7 man
\end{verbatim}

La commande \verb+man+ a deux pages de manuels, dans les sections
1. Commandes utilisateur et 7. Divers.

En cas de doute il suffit de se référer à l'en-tête de la page de
manuel qui précise de quelle commande et entre parenthèses de quel
numéro de section il s'agit~:
\begin{verbatim} MAN(1) Utilitaires de l’afficheur des pages de manuel
MAN(1)
\end{verbatim}

\subsection{Info} Info est une alternative à \texttt{man}.

Elle est au format Hypertexte, qui permet de relier les pages d'aides
en elles.

C'est la seule alternative à \texttt{man} qui a survécu et qui est
encore régulièrement utilisée.

Pour naviguer dans info, les commandes sont les suivantes~: 

\begin{itemize}
\item Aller à la page suivante~: \texttt{n}
\item Aller à la page précédente~: \texttt{p}
\item Remonter dans l'arborescence~: \texttt{u}
\item Remonter à la dernière page visitée~: \texttt{l}
\item Suivre un lien (mot précédé de *)~: \texttt{appuyez sur
Entrée}
\end{itemize}

Pour appeler une page d'\texttt{info}, la commande est la suivante~:
\texttt{info \textbf{nom\_du\_programme}}

\subsection{-{}-help ou -h}

Cette méthode ne fonctionne pas avec toutes les commandes, mais
peut-être utile.  Un grand nombre de programmes répond à l'argument
\texttt{-{}-help} ou \texttt{-h} en renvoyant une page d'aide dans le
terminal.

Cependant, cela n'est pas standard, et il est notamment possible que
l'argument court \texttt{-h} corresponde à autre chose qu'à
l'aide. Comparez par exemple le résultat des commandes suivantes~:
\begin{verbatim} man -h man --help ls -h ls --help
\end{verbatim}




