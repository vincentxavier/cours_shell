\chapter{Utilisation avancée}
Cette partie présente une utilisation plus avancée du sehll, interface
«naturelle» entre l'homme et la machine. On y présente plus en détails
l'édition de fichiers, ou encore la recherche d'informations dans ces
fichiers, ou leur création.


\section{Édition en curses}

Le terme \og curses\fg\ désigne une bibliothèque d'affichage pour les
terminaux Unix très utilisée.

Jusqu'a présent, nous avons vu qu'un terminal sait essentiellement
afficher une succession d'invites, de commandes et de résultats de ces
commandes.  Mais l'affichage dans un terminal ne se limite pas à
ça. Il est tout à fait possible d'y éditer des fichiers. Plusieurs
éditeurs textes utilisent curses pour fournir cette possibilité.

Les éditeurs \textbf{nano} et \textbf{vim} comptent parmi ceux-ci et
vont vous être présentés.

Remarque : il existe d'autres éditeurs de texte fonctionnant dans un
terminal, tel que GNU Emacs, qui a été utilisé avec vim pour la
réalisation de ce document.

\subsection{Nano}

\textbf{Nano} est un \og clone\fg\ libre de l'éditeur pico\footnote{On
  trouve ici une blague de geek, nano étant plus grand que pico, il
  est donc mieux}. C'est un
éditeur texte en curses dont voici une capture d'écran.
\begin{center} {\includegraphics[width=10cm]{nano}}
\end{center}

La partie inférieure de l'écran est constituée d'une barre indiquant
les raccourcis claviers, ce qui permet d'utiliser \textbf{nano} sans
avoir à rechercher plus de documentation.

Le signe \^{} dans chaque raccourci désigne une combinaison utilisant
la touche \emph{Ctrl} du clavier. Par exemple, \og\^{}G\fg\ correspond
à la combinaison Ctrl+G.

\subsection{Vim}

\textbf{Vim} est l'éditeur en curses par excellence, il est léger et
extrêmement puissant. C'est un vrai couteau suisse.

Cependant, sa complexité relative et sa difficulté d'accès pour les
néophytes font qu'il n'est pas utilisé en premier choix par les non
initiés, qui se tourneront plutôt vers \texttt{nano}.

Voici un screenshot de Vim.

\begin{center} {\includegraphics[width=10cm]{vi}}
\end{center}

Vim a un fonctionnement un peu particulier, en effet lors de son
lancement il est en mode \textbf{commande}. Les lettres que vous
entrez alors ne sont pas ajoutées au document, mais les commandes
correspondantes sont exécutées.

Pour passer en mode insertion et pouvoir insérer du texte, il faut
utiliser une commande d'insertion comme la touche \texttt{i} ou
\texttt{a} de son clavier.

Pour sortir du mode insertion et revenir en mode commande (et donc
pouvoir taper les commandes indiquées ci-dessous), il faut appuyer sur
la touche $<$Echap$>$ du clavier.

Voici un tout petit apperçu des commandes qui peuvent être utilisées
dans \textbf{vim}.
\begin{itemize}
\item \texttt{i} et \texttt{a}~$\rightarrow$ insérer du texte avant et
après le curseur
\item $<$Echap$>$~$\rightarrow$ sortir du mode insertion
\item \texttt{X} et \texttt{x}~$\rightarrow$ supprimer le caractère
avant le curseur et le caractère sous le curseur
\item \texttt{o}~$\rightarrow$ ajouter une ligne vide
\item \texttt{J}~$\rightarrow$ joindre le contenu de la suivante à la
fin de la ligne courante
\item \texttt{dd}~$\rightarrow$ supprimer la ligne courante
\end{itemize}

Les commandes de gestion de fichiers sont précédées de \og:\fg\ et
doivent être validées avec la touche $<$Entrée$>$~:
\begin{itemize}
\item \texttt{:w}~$\rightarrow$ enregister le fichier.
\item \texttt{:q}~$\rightarrow$ quitter le programme.
\item \texttt{:q!}~$\rightarrow$ quitter le programme sans enregistrer
les modifications.
\item \texttt{:wq} ou \texttt{:x}~$\rightarrow$ enregister le fichier
et quitter le programme.
\end{itemize}
\newpage

\section{Recherche}

\subsection{Dans le système de fichiers}

Il existe plusieurs façons d'effectuer des recherches, mais nous
n'allons en voir que deux.  L'une avec \texttt{find}, qui effectue la
recherche au moment où l'on exécute la commande et l'autre avec le
couple \texttt{locate} et \texttt{updatedb} qui effectuent une
recherche dans une base de données pré-générée.

\subsubsection{find}

\normalsize{Find exécute une recherche directe sur le disque, il
permet donc d'avoir des résultats fiables au prix d'un temps de
recherche plus élevé. Son utilisation est plus compliquée que
\texttt{locate} que nous verrons juste après, mais en contrepartie
\texttt{find} est plus puissant.

Pour rechercher le fichier \og test\fg\ dans le dossier /home (et ses
sous dossiers)~: \texttt{find /home -name test}

L'atout de find, c'est la quantité d'options qu'il accepte, permettant
de trouver des fichiers en utilisant des critères très précis.

Par exemple, vous pouvez chercher un fichier avec le même nom que dans
l'exemple ci-dessus, mais dont la dernière modification a été
effectuée dans les 3 derniers jours~: \texttt{find /home -name test
-mtime 3}

}

\subsubsection{locate et updatedb}

\normalsize{\texttt{locate} et \texttt{updatedb} travaillent de
conserve.

En effet, \texttt{locate} recherche la chaîne donnée en argument dans
la base de données créée et mise à jour par \texttt{updatedb}.

Leur utilisation est simple~:
\begin{itemize}
\item \texttt{locate}~: \texttt{locate [fichier\_recherché]}
\item \texttt{updatedb}~: \texttt{sudo updatedb}
\end{itemize}

\textbf{Remarques~:}
\texttt{locate} est très rapide à donner ses résultats puisqu'au
lieu de rechercher sur le disque, il lui suffit d'accéder à la base de
données créée par \texttt{updatedb}.

 L'inconvénient est que les changements dans le système de
fichier qui ont eu lieu depuis le dernier appel de \texttt{updatedb}
ne sont pas pris en compte lorsqu'on appelle
\texttt{locate}. Généralement, l'indexation du système de fichier par
\texttt{updatedb} a lieu la nuit ou lorsqu'on rallume
l'ordinateur. Ce lancement est géré par l'ordonanceur de tâche :
\texttt{cron} et son compagnon \texttt{anacron}.


\subsection{Dans un fichier texte}

Pour effectuer ce type de requète, il existe plusieurs utilitaires, le
plus accessible de tous est \texttt{grep}.

Pour information, il existe aussi \texttt{awk}, qui est très évolué,
et qui permet de faire beaucoup plus de traitement mais dont la
syntaxe est beaucoup plus complexe.

L'utilisation de grep est assez simple~:
\begin{itemize}
\item Rechercher les lignes qui répondent au \texttt{motif} dans le
\texttt{fichier}~: \texttt{grep \textbf{motif} \textbf{fichier}}
\item Rechercher les lignes qui ne répondent pas au \texttt{motif}
dans le \texttt{fichier}~: \texttt{grep -v \textbf{motif}
\textbf{fichier}}
\end{itemize}

Le texte que nous utiliserons est le refrain de l'hymne français, à
savoir la Marseillaise dont le texte contenu dans le fichier nommé
\emph{\og marseillaise\fg} est le suivant~: \emph{Aux armes, citoyens
!}  \emph{Formez vos bataillons !}  \emph{Marchons, marchons !}
\emph{Qu'un sang impur...}  \emph{Abreuve nos sillons !}

La commande \texttt{grep armes marseillaise} retournera~: \emph{Aux
armes, citoyens !}.

La commande \texttt{grep -v armes marseillaise} retournera~:
\emph{Formez vos bataillons !}  \emph{Marchons, marchons !}
\emph{Qu'un sang impur...}  \emph{Abreuve nos sillons !}

\section{Flux et redirections} Une particularité qui rend les shells
textuels particulièrement puissants est leur capacité à enchaîner les
commandes et à les faire interagir. C'est là qu'intervient la notion
de flux.

Dans les shells, ces flux sont au nombre de trois. \`{A} savoir 1 flux
d'entrée, et deux flux de sortie.
\begin{itemize}
\item Le flux d'entrée, qui sert à la saisie de caractères au clavier,
aussi appelé entrée standard.
\item Le flux de sortie 1, qui sert à l'affichage des données, aussi
appelé sortie standard.
\item Le flux de sortie 2, qui sert à l'affichage des erreurs.
\end{itemize}

Pour traiter ces flux, il existe plusieurs systèmes.

\subsection{Le chevron, ou \og$>$\fg}
\begin{itemize}
\item Seul, il permet de rediriger le flux de sortie 1 vers une
destination (exemple~: \og echo truc $>$ /dev/null\fg\ enverra le flux
de sortie 1 de la commande \og echo truc\fg\ vers le fichier spécial
/dev/null qui est un \og trou noir\fg, donc n'affichera rien).
\item Adjoint d'un numéro, il ne redirige que la sortie demandée
(exemple~: \og ls /dossierbidon 2$>$ /dev/null\fg\ enverra le flux
d'erreur de la commande \og ls /dossierbidon\fg\ vers /dev/null, cette
commande n'affichera donc rien, mais ne s'est pas exécutée
correctement.
\item Quand le chevron est mis deux fois à la suite,
\emph{$>$}\emph{$>$}, la sortie est écrite dans le fichier sans
l'écraser, mais en le continuant.
\end{itemize}

\subsection{Le chevron en pratique} Vous savez déjà que la commande
\texttt{ls} permet d'afficher le contenu d'un dossier. Comment faire
si l'on veut conserver cette liste, ou l'envoyer à quelqu'un par
exemple~?  Rien de plus simple, il suffit d'utiliser une redirection
pour écrire le résultat de \texttt{ls} dans un fichier~!  La commande
suivante écrit le contenu du dossier courant dans un fichier appelé
\texttt{liste.txt}~:

\prompt{ls > liste.txt}

Vous pouvez vérifier qu'un fichier \texttt{liste.txt} a été créé dans
le dossier courant. Vous pouvez alors editer ce fichier à l'aide d'une
des méthodes abordées précédemment (\texttt{cat},
\texttt{nano}...). Vous constaterez qu'il contient bien le résultat de
la commande \texttt{ls} appelée seule... plus le nom du fichier
\texttt{liste.txt} qui a été créé avant l'appel effectif de ls !

\subsection{Le tube ou «pipe»~: $|$}

Le tube est un peu fourbe, au moins pour les claviers français. Pour
l'obtenir, il faut combiner \textbf{Alt Gr} et la touche du tiret
\textbf{-} et du 6, au dessus du clavier alphabétique.

La syntaxe \texttt{commande1 | commande2} permet à la
\texttt{commande2} de \og travailler\fg\ sur ce que la
\texttt{commande1} renvoie habituellement en sortie 1 dans le
terminal.  On connecte en fait la sortie standard de la commande1 sur
l'entrée standard de la commande2.  Ainsi~:
\begin{itemize}
\item au lieu d'écrire sa sortie dans le terminal, la commande1
l'écrit dans le tube~;
\item au lieu de lire la saisie de l'utilisateur, la commande2 lit son
entrée dans le tube.
\end{itemize}

\subsection{Le tube en pratique} 

Supposons que nous voulions lire la sortie d'un programme comme
\texttt{dmesg} qui affiche le journal du noyau sur la sortie
standard. Cette sortie comporte en général plus de 1000 lignes et les
terminaux ne peuvent en général l'afficher. Pour cela, il est
pertinent de rediriger la sortie de \texttt{dmesg} dans \texttt{less}
en tapant 

\texttt{dmesg | less}. 

Remarque : nous aurions aussi bien pu choisir \texttt{more} comme
visualiseur. 

Une autre utilisation peut être la recherche d'une chaîne particulière
dans la sortie de dmesg. Pour cela, nous aurions pu rediriger la
sortie de \texttt{dmesg} dans un fichier puis chercher la chaîne en
question dans le fichier. Nous allons utiliser la commande
\texttt{grep} d'une façon différente. En effet, le comportement de
cette commande n'est pas le même lorsqu'on ne lui donne pas de nom de
fichier comme deuxième argument. Dans ce cas, \texttt{grep} utilisae
par défaut l'entrée standard.\par 
Utilisons par exemple la commande suivante~:

\texttt{grep in}

On peut alors saisir du texte, et \texttt{grep} ne va rendre sur la
sortie standard que les lignes qui contiennent \og in\fg\ (Ctrl+C pour
arrêter)~:
\begin{verbatim} Quel est ton système préféré ?<Entrée> C'est
GNU/Linux !<Entrée> C'est GNU/Linux !
\end{verbatim}

Remarque : Pour indiquer que nous avons terminé de lire l'entrée
standard (ou un fichier, ce qui revient au même), il faut envoyer le
caractère fin de fichier (en anglais end-o-file ou EOF) qui s'obtient
généralement avec la commande \texttt{Ctrl+d}.

C'est amusant mais pas très longtemps... Ce qui est en revanche
beaucoup plus intéressant, c'est d'utiliser \texttt{grep} pour
analyser la sortie d'une autre commande.  Reprennons notre exemple
avec \texttt{dmesg}. \texttt{dmesg} permet entre autre de savoir quel
options ont été passsé au noyau lors du démarrage. Pour cela, il faut
retrouver la ligne contenant la chaine \texttt{command line}.

\texttt{dmesg | grep 'command line'}

Le concept de \og pipe\fg\ est d'autant plus puissant qu'il est
extrêmement flexible. On peut ainsi enchaîner plus de deux
commandes. Par exemple, la commande qui suit utilise \texttt{wc -l}
pour compter les lignes du résultat de l'exemple précédent~:

\texttt{dmesg $|$ grep -i irq $|$ wc -l}

Dans les conditions de l'exemple précédent, cette commande retournera
simplement : \texttt{41}

\section{Les expressions rationelles (Regular Expressions ou REGEX)}

Dans son livre Digital Typography, paru en 1999, Donald Knuth disait
\og I define UNIX as 30 definitions of regular expressions living
under one roof.\fg, qu'on pourrait traduire par \og Je défini UNIX
comme 30 définitions d'expressions rationelles cohabitant sous le même
toît\fg.

Sous ce nom barbare se cache une syntaxe permettant de manipuler du
texte très efficacement.

La base syntaxique est très similaire, seules quelques légères
différences sont à remarquer entre les différents langages de
programmation ayant implémentés les REGEX.

Ici, nous allons parler des REGEX sous bash.

\subsection{Les principes de base}

Une expression rationelle est composée d'une suite de caractères
appelée \og motif\fg ou \og pattern\fg en anglais, qui est utilisée
pour décrire une chaine de caractère avec pour but de la retrouver
dans un texte.

Ensuite, cette recherche peut être utilisée pour lui appliquer un
traitement automatisé, comme un ajout, une suppression ou un
remplacement.

\subsection{Syntaxe des expressions rationnelles}

\subsubsection{Les caractères}
\begin{itemize}
\item Le point \verb+.+ capture un caractère quelconque. Ainsi
l'expression \verb+mo.+ correspond par exemple à «~moi~», «~mon~» ou
«~mot~».
\item Une suite de caractères entre crochets recherche l'un de ces
caractères. \verb+p[aeiouy]+ recherche donc une syllabe constituée de
la lettre p suivie d'une voyelle.
\end{itemize}

\subsubsection{Le \og pipe\fg, ou \og$|$\fg}

Il est utilisé pour séparer deux expressions alternatives, comme par
exemple \verb+bash|shell+ correspond soit au mot «~bash~» soit au mot
«~shell~».

\subsubsection{Les quantificateurs}

Ces caractères permettent de spécifier combien d'occurrences d'une
expression ou d'un caractère on cherche.

\begin{itemize}
\item \verb+?+ recherche le groupe ou le caractère zéro ou une fois~:
\verb+shell?+ correspond à «~shel~» ou «~shell~»
\item \verb#+# recherche le groupe ou le caractère une fois ou plus~:
\verb#shell+# peut correspondre à «~shell~» ou «~shelll~» ou
«~shelllllllllllll~»
\item \verb+*+ recherche le groupe ou le caractère zéro, une ou
plusieurs fois : \verb+shell*+ peut correspondre à «~shel~» ou
«~shell~» ou «~shelllllllllllll~»
\item \verb+{n}+ recherche le groupe ou le caractère exactement n
fois~: \verb+(cou){2}+ correspond à «~coucou~»
\item \verb+{n,m}+ recherche au minimum n et au maximum m fois le
groupe ou le caractère. Si la valeur n (resp. m) est omise – mais pas
la virgule – alors il n'y a pas de limite minimum (resp. maximum) au
nombre d'occurrences recherchées.
\end{itemize}

\subsubsection{Opérateurs propres à grep, ed, sed et vi}

Ces opérateurs ne sont utilisés que par ces programmes.
\begin{itemize}
\item \texttt{\og \^\fg} signifie que l'expression qui le suit doit
être en début de ligne.
\item \texttt{\og \$\fg} signifie que l'expression qui le précède doit
être en fin de ligne.
\end{itemize} Les caractères \og ?\fg, \og +\fg,\og *\fg ont la même
signification que ceux définis précédement.

\section{Utilisation des expressions rationnelles} Un certain nombre
de commandes reconnaissent les expressions rationnelles. C'est
notamment le cas de \verb+grep+ avec l'option \verb+-e+ et de
\verb+sed+.

C'est également le cas d'un afficheur de page tel que \texttt{less}
qui comprend ces expressions rationnelles. La recherche dans
\texttt{less} s'obtient en tapant \texttt{/motif}.g

L'éditeur Vim comprend également ces expressions dans ses fonctions de
recherche et de remplacement. Cependant certains caractères des
expressions rationnelles ont besoin d'être échappés par un antislash
pour conserver leur fonction sinon ils sont recherchés
littéralement. C'est le cas de \verb+\{+ et \verb+\}+ notamment.


