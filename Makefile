TEX=$(wildcard bouclette*.tex)
PDF=$(TEX:.tex=.pdf)

COURS_TEX=$(wildcard cours*.tex)
COURS_PDF=$(COURS_TEX:.tex=.pdf)

PDFTEX = pdflatex -interaction nonstopmode

EXT = aux log lof lot out toc tns nav snm old blg bbl

TEMP=$(foreach ext,$(EXT), $(foreach file,$(TEX), $(file:.tex=.$(ext))))

NEED_RERUN = '(There were undefined references|Rerun to get (cross-references|the bars) right)|((Reference|Citation).*undefined)|(Label.*multiply defined)'

all : ${PDF}

cours : ${COURS_TEX}
	@echo 'Compilation de $@'
	${PDFTEX} $<
	if [ -e $*.bib ] ; then if egrep -q $(NEED_RERUN) $*.log ; then bibtex $* ; fi ; fi
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} $< ; fi 
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} $< ; fi 


notes :	${COURS_TEX}
	@echo 'Compilation des notes de présentation'
	${PDFTEX} "\def\notesinclude{}\input{$<}"
	if [ -e $*.bib ] ; then if egrep -q $(NEED_RERUN) $*.log ; then bibtex $< ; fi ; fi
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} "\def\notesinclude{}\input{$<}" ; fi 
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} "\def\notesinclude{}\input{$<}" ; fi

%.pdf : %.tex 
	@echo 'Compilation de $@'
	${PDFTEX} $<
	if [ -e $*.bib ] ; then if egrep -q $(NEED_RERUN) $*.log ; then bibtex $* ; fi ; fi
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} $< ; fi 
	if egrep -q $(NEED_RERUN) $*.log ; then ${PDFTEX} $< ; fi 

clean : 
	@echo 'Nettoyage ...'
	@ ${RM} ${TEMP}

mrproper : clean
	@echo 'Effacement des cibles : ${PDF}'
	@ ${RM} ${PDF}
